import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllGames } from './all-games';

@NgModule({
  declarations: [
    AllGames,
  ],
  imports: [
    IonicPageModule.forChild(AllGames),
  ],
  exports: [
    AllGames
  ]
})
export class AllGamesModule {}
