import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from 'jquery';

/**
 * Generated class for the AllGames page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-all-games',
  templateUrl: 'all-games.html',
})
export class AllGames {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    $(function blackOrRed(){
      $("#blackOrRed").click(function(){
        $(".jagge").toggleClass("jagge-active")
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllGames');
  }

}
