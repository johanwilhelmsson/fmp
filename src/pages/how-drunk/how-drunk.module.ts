import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HowDrunk } from './how-drunk';

@NgModule({
  declarations: [
    HowDrunk,
  ],
  imports: [
    IonicPageModule.forChild(HowDrunk),
  ],
  exports: [
    HowDrunk
  ]
})
export class HowDrunkModule {}
