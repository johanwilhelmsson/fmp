import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Recommend } from '../recommend/recommend';
import { StageTwoHome } from '../stage-two-home/stage-two-home';
import { StageThreeHome } from '../stage-three-home/stage-three-home';
import { StageFourHome } from '../stage-four-home/stage-four-home';
import { StageFiveHome } from '../stage-five-home/stage-five-home';
import { PointingGame } from '../pointing-game/pointing-game';
import { AllGames } from '../all-games/all-games';


import * as $ from 'jquery';


/**
 * Generated class for the HowDrunk page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-how-drunk',
  templateUrl: 'how-drunk.html',
})

export class HowDrunk
{

  // set the drunkness starting level to 0
  drunkness = 0;

  // constructor(public navCtrl:NavController, public drunkness:any)
  constructor(public navCtrl:NavController)
  {

  }

  navHomePage()
  {
    this.navCtrl.push(HomePage);
  }

  clickedGo()
  {
    console.log('clicked GO');
    console.log(this.drunkness);
    // console.log(drunkness);
    if (this.drunkness >-1 && this.drunkness <20)
      {
        this.navCtrl.push(HomePage);
      }
    else if (this.drunkness >20 && this.drunkness <40)
      {
          this.navCtrl.push(StageTwoHome);
      }
    else if (this.drunkness >40 && this.drunkness <60)
        {
          this.navCtrl.push(AllGames);
        }
    else if (this.drunkness >60 && this.drunkness <80)
          {
            this.navCtrl.push(StageFourHome);
          }
    else if (this.drunkness >80 && this.drunkness <101)
          {
            this.navCtrl.push(StageFiveHome);
          }
  }

  displayText() {
    console.log('Its working')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HowDrunk');
  }

}
