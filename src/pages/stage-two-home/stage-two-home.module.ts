import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StageTwoHome } from './stage-two-home';

@NgModule({
  declarations: [
    StageTwoHome,
  ],
  imports: [
    IonicPageModule.forChild(StageTwoHome),
  ],
  exports: [
    StageTwoHome
  ]
})
export class StageTwoHomeModule {}
