import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HowDrunk } from '../how-drunk/how-drunk';

import * as $ from 'jquery';

/**
 * Generated class for the StageTwoHome page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-stage-two-home',
  templateUrl: 'stage-two-home.html',
})
export class StageTwoHome {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    $(function menuNav() {
      $(".menu-collapsed").click(function() {
        $(this).toggleClass("menu-expanded");
      });
    });

    $(function showText(){
      $('#showText').click(function(){
        $('#textShow').toggleClass('participants-active');
        $('.fabPop, .logLog , #recFont, #popFont, #signFont').toggleClass('hideItem')
        $('.fabRec').toggleClass('moveAllStuff')
      });
    });

    $(function (){
      $('.popPop').click(function(){
        $('#showText, .logLog, #recFont, #popFont, #signFont').toggleClass('hideItem');
        $('.fabPop').toggleClass('moveAllStuff');
      });
    });

    $(function (){
      $('.logLog').click(function(){
        $('.fabRec, .fabPop, .popPop, #recFont, #popFont, #signFont').toggleClass('hideItem');
        $('.fabLog').toggleClass('moveItem');
        $('.logInDetails').toggleClass('logInDetails-active');
      });
    });

    $(function (){
      $('#oneRec').click(function(){
        $('#textShowAgain').toggleClass('participants-activeAgain');
        $('#showText, #textShow, .twoRec, .threeRec').toggleClass('hideItemAgain');
        $('.recOne').toggleClass('moveAllStuffAgain');
      });
    });

    $(function (){
      $('.twoRec').click(function(){
        $('#textShowAgain').toggleClass('participants-activeAgain');
        $('#showText, #textShow, #oneRec, .threeRec').toggleClass('hideItemAgain');
        $('.recTwo').toggleClass('moveFromLeft');
      });
    });

    $(function (){
      $('.threeRec').click(function(){
        $('#textShowAgain').toggleClass('participants-activeAgain');
        $('#showText, #textShow, #oneRec, .twoRec').toggleClass('hideItemAgain');
        $('.recThree').toggleClass('moveFromRight');
      });
    });


  }

  navHowDrunk()
  {
    this.navCtrl.push(HowDrunk);
  }

  navAllGames()
  {
    this.navCtrl.push(HowDrunk);
  }

  navSignIn()
  {
    this.navCtrl.push(HowDrunk);
  }

  navStageTwoHome()
  {
    this.navCtrl.push(HowDrunk);
  }






  ionViewDidLoad() {
    console.log('ionViewDidLoad StageTwoHome');
  }

}
