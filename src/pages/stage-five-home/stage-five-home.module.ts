import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StageFiveHome } from './stage-five-home';

@NgModule({
  declarations: [
    StageFiveHome,
  ],
  imports: [
    IonicPageModule.forChild(StageFiveHome),
  ],
  exports: [
    StageFiveHome
  ]
})
export class StageFiveHomeModule {}
