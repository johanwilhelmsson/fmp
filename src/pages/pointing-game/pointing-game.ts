import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as $ from 'jquery';

/**
 * Generated class for the PointingGame page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pointing-game',
  templateUrl: 'pointing-game.html',
})
export class PointingGame {

deltaX = null;
startX = null;

  constructor(public navCtrl: NavController) {
}


Swipe(){
  var deltaX = null;
  var startX = null;

    document.querySelector('.container').addEventListener('touchstart',function(e){
      deltaX = e.targetTouches[0].pageX;

      this.addEventListener('touchmove',function(e){
        console.log(deltaX);
      },false);
    },false);

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad PointingGame');
  }



}
