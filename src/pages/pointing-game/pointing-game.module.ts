import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PointingGame } from './pointing-game';

@NgModule({
  declarations: [
    PointingGame,
  ],
  imports: [
    IonicPageModule.forChild(PointingGame),
  ],
  exports: [
    PointingGame
  ]
})
export class PointingGameModule {}
