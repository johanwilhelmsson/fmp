import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StageFourHome } from './stage-four-home';

@NgModule({
  declarations: [
    StageFourHome,
  ],
  imports: [
    IonicPageModule.forChild(StageFourHome),
  ],
  exports: [
    StageFourHome
  ]
})
export class StageFourHomeModule {}
