import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Recommend } from '../recommend/recommend';
import { HowDrunk } from '../how-drunk/how-drunk';

import * as $ from 'jquery';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  constructor(public navCtrl: NavController) {

  var recommendOriginalHTML = $('#recommend').html();


  $(function menuNav() {
    $(".menu-collapsed").click(function() {
      $(this).toggleClass("menu-expanded");
    });
  });





    $(function signIn() {
      $("#signIn").click(function() {
        $('#signIn').toggleClass('transform-active');
        $('#signIn').html('<b>Det fungerar</b>');
        });
      });


    $(function recommend() {
      $("#recommend").click(function() {
        $('#recommend').toggleClass('transform-active');
        $('#recommend').html('<h3>Number of participants</h3><li><button class="Buttons">1-3</button></li> <li><button class="Buttons">3-5</button></li> <li><button class="Buttons">5-7</button></li> <li><button class="Buttons">7+</button></li> ');
        $('#recommend li').css({'list-style-type': 'none', 'margin-top': '2em', 'left': '0%', 'position': 'relative', 'font-size': '1.5em' });

      });
    });

    $(function allGames() {
      $("#allGames").click(function() {
        $('#allGames').toggleClass('transform-active-mid');
      });
    });

    $(function popular() {
      $("#popular").click(function() {
        $('#popular').toggleClass('transform-active-mid');
      });
    });

    $(function settings() {
      $("#settings").click(function() {
        $('#settings').toggleClass('transform-active');
      });
    });

    $(function drinkSafe() {
      $("#drinkSafe").click(function() {
        $('#drinkSafe').toggleClass('transform-active');
      });
    });





    }
  nextPage(){
    this.navCtrl.push(Recommend);
  }


}
