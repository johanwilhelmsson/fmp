import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StageThreeHome } from './stage-three-home';

@NgModule({
  declarations: [
    StageThreeHome,
  ],
  imports: [
    IonicPageModule.forChild(StageThreeHome),
  ],
  exports: [
    StageThreeHome
  ]
})
export class StageThreeHomeModule {}
