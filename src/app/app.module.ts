import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { SwingModule } from 'angular2-swing';
import { Flashcard } from '../components/flashcard/flashcard';




import { Recommend } from '../pages/recommend/recommend';
import { HowDrunk } from '../pages/how-drunk/how-drunk';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StageTwoHome } from '../pages/stage-two-home/stage-two-home';
import { StageThreeHome } from '../pages/stage-three-home/stage-three-home';
import { StageFourHome } from '../pages/stage-four-home/stage-four-home';
import { StageFiveHome } from '../pages/stage-five-home/stage-five-home';
import { PointingGame } from '../pages/pointing-game/pointing-game';
import { AllGames } from '../pages/all-games/all-games';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Recommend,
    HowDrunk,
    StageTwoHome,
    StageThreeHome,
    StageFourHome,
    StageFiveHome,
    PointingGame,
    Flashcard,
    AllGames
  ],
  imports: [
    BrowserModule,
    HttpModule,
   SwingModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Recommend,
    HowDrunk,
    StageTwoHome,
    StageThreeHome,
    StageFourHome,
    StageFiveHome,
    PointingGame,
    Flashcard,
    AllGames
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
