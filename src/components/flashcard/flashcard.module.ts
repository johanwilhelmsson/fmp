import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Flashcard } from './flashcard';

@NgModule({
  declarations: [
    Flashcard,
  ],
  imports: [
    IonicPageModule.forChild(Flashcard),
  ],
  exports: [
    Flashcard
  ]
})
export class FlashcardModule {}
