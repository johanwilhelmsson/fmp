import { Component } from '@angular/core';


@Component({
  selector: 'flashcard',
  templateUrl: 'flashcard.html'
})
export class Flashcard {

  flipped: boolean = false;

   constructor() {

   }

   flip(){
     this.flipped = !this.flipped;
   }

}
